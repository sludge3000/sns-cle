# SNS-CLE

The Stormshield Network Security Connection Log Enumerator has been written to 
allow easy extraction of specific logs from the l_connection log files of the 
SNS system.

Usage:
Copy the snscle.py script into the same folder as the connection log files.
Run the script from a command prompt.
You will be prompted for a destination IP address and date on which to filter.
A file be created in the same folder with the destination IP included in the 
file name.

NOTE:
SNS connection logs are written when a connection is closed. In order to ensure 
you obtain all applicable logs, we recommend you includes at least 1 hour of 
log files before and after the date you are searching for.