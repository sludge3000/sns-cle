#!/usr/bin/env python3.7
#
# Stormshield Network Security Connection Log Enumerator
# Version: 0.1 - BETA
# Created by: Luke "sludge3000" Savage
#

# Import modules
import argparse
import os
from pathlib import Path


# Initialize global tables
templist1 = []
templist2 = []


def tlc():
    global templist1
    global templist2
    templist1 = templist2
    templist2 = []


def iter(filter):
    global templist1
    global templist2
    for line in templist1:
        if filter in line:
            templist2.append(line)


def extract(files):
    for file in files:
        iFile = open(file)
        for line in iFile:
            templist2.append(line)


def filter_date(date):
    date = 'startime="' + date
    tlc()
    iter(date)


def filter_sourceip(sourceip):
    source = 'src=' + sourceip
    tlc()
    iter(source)


def filter_destip(destip):
    dest = 'dst=' + destip
    tlc()
    iter(dest)


def output(oFile):
    output = open(oFile, 'w+')
    for line in templist2:
        output.write(line)
    output.close()


def snscle():
    # Set local ariables
    p = Path(os.getcwd())
    files = list(p.glob('l_connection*'))

    # Set argparse Variables
    args = parser.parse_args()
    date = args.date
    sourceip = args.srcip
    destip = args.dstip
    oFile = 'StaticName.txt'

    extract(files)

    if date is not None:
        filter_date(date)

    if sourceip is not None:
        filter_sourceip(sourceip)

    if destip is not None:
        filter_destip(destip)

    output(oFile)


# Argparse configuration
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
parser.add_argument("-D", "--date", default=None,
                    help="Specify the date filter in the format YYYY-MM-DD")
parser.add_argument("-s", "--srcip", default=None,
                    help="Specify source IP to filter.")
parser.add_argument("-d", "--dstip", default=None,
                    help="Specify destination IP to filter.")


def main():
    snscle()


main()
